NOW := $(shell date +"%c" | tr ' :' '__')

release:
	zip -x \*.zip .\* package.json node_modules/\* Makefile screenshots/\* README.md -r release_$(NOW).zip .

clean:
	rm *.zip

.PHONY: clean
