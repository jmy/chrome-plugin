/* global chrome, Trello, $ */

// Event callback from Extension, fires each time a new message is received.
// In this case, a message is when the URL is updated, in other words, when a
// Card is "opened" and "closed".
chrome.extension.onMessage.addListener(function (req, sender, sendResponse) {
  if (req.command === 'addmarkup') {
    HeseForTrello.addCardMarkup()
  }
})

var HeseForTrello = {

  sidebar: '.window-sidebar',
  card: $('.active-card'),
  boards: [
    {
      name: 'Työlista',
      id: '557fd70ec1d7c8643e0b32eb',
      urlId: 'wp5n7vLN',
      fullName: 'Hesburger työlista',
      selector: '.js-platform-tyolista'
    }, {
      name: 'Android',
      id: '55dedfb7ea0beab7e1cd3b16',
      urlId: 'GIqeIYbj',
      fullName: 'Hesburger työlista, Android',
      selector: '.js-platform-android',
      targetListId: '569f5cff8eb1e71f1bfc239a'
    }, {
      name: 'iOS',
      id: '58a455eb2c77aa892b0b05c9',
      urlId: 'KeeTNPEQ',
      fullName: 'Hesburger työlista, iOS',
      selector: '.js-platform-ios',
      targetListId: '58ad6b9dc5572eaeba4eeb08'
    }, {
      name: 'Backend',
      id: '55473c2c4aeb93c941d1b67d',
      urlId: 'KcpljW89',
      fullName: 'Hesburger Backend',
      selector: '.js-platform-backend',
      targetListId: '55473c2c4aeb93c941d1b67f'
    }, {
      name: 'Päätökset',
      id: '55f7e278562271ae077ce7ef',
      urlId: '8O4mY3cR',
      fullName: 'Hesburger Sprintit ja päätökset',
      selector: '.js-platform-decisions',
      targetListId: '55f7e2818e8909033e55e46c'
    }
  ],

  cardName: function () {
    return $('.card-detail-window .js-title-helper').first().text()
  },

  authenticate (onSuccess) {
    if (Trello.authorized()) {
      onSuccess()
      return
    }

    const authenticationSuccess = () => {
      console.log('Successful authentication')
      onSuccess()
    }
    const authenticationFailure = () => {
      console.log('Failed authentication')
      window.alert('Trello authentication failed')
    }

    Trello.authorize({
      type: 'popup',
      name: 'Hese for Trello',
      scope: {
        read: 'true',
        write: 'true'
      },
      expiration: 'never',
      success: authenticationSuccess,
      error: authenticationFailure
    })
  },

  addCardMarkup () {
    var url = window.location.href
    var urlType = url.split('trello.com/')
    urlType = urlType[1][0] // expects b or c
    if (urlType.toLowerCase() !== 'c') { return }

    const boardName = document.title.split('on').pop().trim().split(' | ')[0].trim()
    const isSupportedTable = this.boards.find((board) => { return boardName === board.fullName })
    if (!isSupportedTable) {
      return
    }

    const otherBoards = this.boards
      .filter((board) => {
        return boardName !== board.fullName
      })

    const sidebar = $(this.sidebar)
    const platformSidebar = $('<div/>', {
      'class': 'platforms-sidebar window-module u-clearfix'
    })
      .append('<h3>Platforms</h3>')
    platformSidebar.tooltip({
      content: 'Tooltip'
    })

    otherBoards.forEach(board => platformSidebar.append(this.platformButton(board)))

    if (sidebar.find('.platforms-sidebar').length === 0) {
      $(this.sidebar).prepend(platformSidebar)

      this.getAllRefCards(otherBoards)
    }
  },

  getAllRefCards (boards) {
    this.boards.forEach(b => { b.refCard = undefined })
    const doGetAllRefCards = () => {
      this.findCards(boards, (cards) => {
        // Prefer shorter matches
        cards.sort((a, b) => {
          return a.name.length - b.name.length
        })
        // Prefer unarchived cards
        cards.sort((a, b) => {
          return a.closed - b.closed
        })
        cards.forEach(c => {
          const board = this.boards.find(b => b.id === c.idBoard)
          if (board) {
            board.refCard = c
          }
        })
        this.boards.forEach(b => {
          $(b.selector).toggleClass('ref-card-exists', !!b.refCard)
        })
      })
    }
    this.authenticate(doGetAllRefCards)
  },

  platformButton (board) {
    var name = board.name
    const button = $('<a/>', {
      'class': 'button-link ' + board.selector.substr(1),
      href: '#',
      click: () => {
        this.openCard(board)
      },
      tooltip: {
        items: '*',
        content: (show) => {
          this.getTooltip(board, (data) => {
            show(data)
          })
        }
      }
    })
      .append('<span class="icon-sm icon-card"/>&nbsp;' + name)
    board.refButton = button
    return button
  },

  getTooltip (board, show) {
    const formatTooltip = (card) => {
      var titleElems = []
      titleElems.push('<strong>List:</strong> ' + card.list.name)
      titleElems.push('<strong>Members:</strong> ' + card.members.map(m => m.fullName).join(', '))
      return titleElems.filter(s => s.length !== 0).join('<br>')
    }
    const errorMessage = `No card found on ${board.name} board.<br>Click to create one.`

    if (board.refCard === undefined) {
      show('Loading...')
      this.getCards(board, (cards) => {
        const card = cards[0]
        board.refCard = card
        show(formatTooltip(board.refCard))
      }, () => {
        board.refCard = null
        show(errorMessage)
      })
    } else if (board.refCard === null) {
      show(errorMessage)
    } else {
      show(formatTooltip(board.refCard))
    }
  },

  getCards (board, onSuccess, onFailure) {
    var doGetCard = () => {
      this.findCards([board], onSuccess, onFailure)
    }
    this.authenticate(doGetCard)
  },

  openCard (board) {
    if (board.refCard) {
      window.open(board.refCard.url)
    } else {
      console.log('No card to navigate to')
      this.addTrelloCardWithPopup(this.cardName(), board)
    }
  },

  findCards (boards, onSuccess, onFailure) {
    const cardName = this.cardName()
    if (!cardName) return
    const boardList = boards.map(b => b.id)

    const searchSuccess = (data) => {
      const cardCount = (data.cards && data.cards.length) || 0
      if (cardCount > 0) {
        onSuccess(data.cards)
      } else {
        searchFailure()
      }
    }

    const searchFailure = (data) => {
      console.log('Failed searching card ' + this.quoted(cardName))
      if (onFailure) {
        onFailure()
      }
    }

    Trello.get('/search/', {
      query: 'name:' + this.quoted(cardName),
      modelTypes: 'cards',
      idBoards: boardList,
      card_fields: ['name', 'url', 'idBoard', 'closed'],
      card_list: true,
      card_members: true
    }, searchSuccess, searchFailure)
  },

  addTrelloCardWithPopup (name, board) {
    const targetListId = board.targetListId

    var url = document.location.href
    var shortUrl = url.substr(0, url.lastIndexOf('/'))
    var desc = 'Tehtävälista-kortti: ' + shortUrl

    window.open('https://trello.com/add-card' +
             '?source=' + window.location.host +
             '&mode=popup' +
             '&url=' + encodeURIComponent(url) +
             (name ? '&name=' + encodeURIComponent(name) : '') +
             '&desc=' + encodeURIComponent(desc) +
             '&idList=' + targetListId,
      '_blank',
      'width=500,height=600,left=' +
        (window.screenX + (window.outerWidth - 500) / 2) +
        ',top=' + (window.screenY + (window.outerHeight - 740) / 2))
  },

  quoted (text) {
    return '"' + text + '"'
  }
}

chrome.extension.sendMessage({}, function () {
  var elm, card, detail, readyStateCheckInterval

  readyStateCheckInterval = setInterval(function () {
    if (document.readyState === 'complete') {
      clearInterval(readyStateCheckInterval)
      chrome.runtime.onMessage.addListener(routeCommand)
    }
  }, 10)

  function routeCommand (request) {
    card = $('.active-card')
    detail = $('.card-detail-window')
    switch (request.command) {
      case 'movecard':
        moveCard()
        break
      case 'movecardup':
        moveCardUp()
        break
      case 'movecarddown':
        moveCardDown()
        break
      case 'copycard':
        copyCard()
        break
      case 'yank':
        yank()
        break
      case 'movecardtop':
        moveCardTop()
        break
      case 'notifications':
        notifications()
        break
      case 'scrolltop':
        scrollTop()
        break
      case 'scrollbottom':
        scrollBottom()
        break
      case 'collapselist':
        collapseList()
        break
      case 'newboard':
        newBoard()
        break
      case 'trello':
        trello()
        break
    }
  }

  function newBoard () {
    $('span.header-btn-icon.icon-lg.icon-add.light').click()

    setTimeout(function () {
      var elm = document.querySelector('a.js-new-board')
      elm.click()
    })
  }

  function moveCard () {
    if (card.length === 1) {
      card.find('span.list-card-operation').trigger('click')
    }

    elm = document.querySelector('a.js-move-card')
    if (elm === null) return

    elm.click()
  }

  function moveCardUp () {
    if (card.length !== 1) return

    var prevCard = card.prev('.list-card')

    if (prevCard) {
      $(card).insertBefore(prevCard)
    }
  }

  function moveCardDown () {
    if (card.length !== 1) return

    var nextCard = card.next('.list-card')

    if (nextCard) {
      $(card).insertAfter(nextCard)
    }
  }

  function copyCard () {
    if (card.length !== 1) return

    card.find('span.list-card-operation').trigger('click')
    elm = document.querySelector('a.js-copy-card')

    elm.click()
  }

  function trello () {
    var authenticationSuccess = function () {
      console.log('Successful authentication')
//      findTrelloCard();
    }
    var authenticationFailure = function () {
      console.log('Failed authentication')
    }

    Trello.authorize({
      type: 'popup',
      name: 'Hese for Trello',
      scope: {
        read: 'true',
        write: 'true'
      },
      expiration: 'never',
      success: authenticationSuccess,
      error: authenticationFailure
    })
  }

  function yank () {
    var url
    var item
    if (card.length === 1) {
      url = $('.list-card-details > a', card)[0].href
      item = card
    } else if (detail.length === 1) {
      url = document.URL
      item = detail
    }

    if (url !== null) {
      url = url.substr(0, url.lastIndexOf('/'))
      console.log('Card:', url)
      flashMessage(item, 'Copied: ' + url)

      chrome.extension.sendMessage({
        text: url
      })
    }
  }

  function moveCardTop () {
    if (card.length !== 1) return

    card.find('span.list-card-operation').trigger('click')
    elm = document.querySelector('a.js-move-card')

    elm.click()
    $('.js-select-position').children().first().attr('selected', 'selected')
    $('input[value="Move"]').click()
  }

  function notifications () {
    document.querySelector('.header-notifications.js-open-header-notifications-menu').click()
  }

  function scrollTop () {
    var cardList = $(':hover').last().parents('.list').children('.list-cards')
    if (cardList) {
      cardList.scrollTop(0)
    }
  }

  function scrollBottom () {
    var cardList = $(':hover').last().parents('.list').children('.list-cards')
    if (cardList) {
      cardList.scrollTop(cardList.height() + 500) // Just to make sure we get the entire height
    }
  }

  function flashMessage (item, message) {
    var position = item.offset()
    if (position == null) return
    $('body').append('<div id="floatingDiv">' + message + '</div>')
    $('#floatingDiv').css({
      'position': 'absolute',
      'top': position.top + 5,
      'left': position.left + (item.width() / 3),
      'color': 'white',
      'background-color': 'rgba(40, 50, 75, 0.65)',
      'padding': '5px',
      'display': 'block',
      'z-index': 9999
    })
      .delay(1000)
      .fadeIn(function () {
        $('#floatingDiv').fadeOut(function () {
          $('#floatingDiv').remove()
        })
      })
  }

  function collapseList () {
    var cardList = $(':hover').last().parents('.list')
    if (!cardList) {
      return
    }

    var prevHeight = cardList.attr('prevHeight')
    if (prevHeight) {
      cardList.animate({
        height: prevHeight
      })
      cardList.removeAttr('prevHeight')
    } else {
      var height = cardList.height()
      cardList.attr('prevHeight', height)
      cardList.animate({
        height: '67px'
      })
    }
  }
})
